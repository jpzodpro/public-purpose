import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {TopBarComponent} from './top-bar/top-bar.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {UserRegisterComponent} from './auth/user-register/user-register.component';
import {ReactiveFormsModule} from '@angular/forms';
import {UserListComponent} from './user/userlist/userlist.component';
import {LoginFormComponent} from './auth/login/loginform.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {UserService} from './shared/services/user.service';
import {AuthInterceptor} from './auth/login/AuthInterceptor';
import {AuthService} from './shared/services/auth.service';


@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    UserRegisterComponent,
    UserListComponent,
    LoginFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [UserService, AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
