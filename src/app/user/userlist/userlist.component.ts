import {Component, OnInit} from '@angular/core';
import {UserService} from '../../shared/services/user.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})
export class UserListComponent implements OnInit {
  public users: any[];

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.userService.list().subscribe(result => {
      console.log(result);
      this.users = result as Object[];
    }, console.error);
  }

}
