import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserListComponent} from './user/userlist/userlist.component';
import {AuthGuard} from './auth/auth.guard';
import {UserRegisterComponent} from './auth/user-register/user-register.component';
import {LoginFormComponent} from './auth/login/loginform.component';
import {UserService} from './shared/services/user.service';

const routes: Routes = [
  {
    path: '',
    component: UserRegisterComponent
  },
  {
    path: 'login',
    component: LoginFormComponent
  },
  {
    path: 'users',
    canActivate: [AuthGuard],
    component: UserListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard, UserService]
})
export class AppRoutingModule { }
