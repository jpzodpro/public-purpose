import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  /**
   * Adds auth header to all HTTP requests if 'token' exists in local storage.
   * This means the user is logged in.
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // if (localStorage.getItem('token') !== null && req.headers.get('Authorization') === null) {
    if (localStorage.getItem('token') != null) {
      console.log('Token: ' + localStorage.getItem('token'));
      console.log('Intercepting HTTP request');
      const customHeaderRequest = req.clone({
        headers: req.headers.set('Authorization', localStorage.getItem('token'))
      });
      return next.handle(customHeaderRequest);
    } else {
      return next.handle(req);
    }
  }

}
