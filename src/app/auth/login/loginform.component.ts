import {Component, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ISubscription} from 'rxjs/Subscription';
import {AuthService} from '../../shared/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.scss']
})
export class LoginFormComponent implements OnDestroy {
  loginSubscription: ISubscription;
  loginForm: FormGroup;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router) {
    this.createForm();
  }

  ngOnDestroy(): void {
    if (this.loginSubscription != null) {
      this.loginSubscription.unsubscribe();
    }
  }

  createForm() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  /**
   * Login with provided credentials
   */
  onSubmit() {
    const credentials = {
      username: this.loginForm.get('username').value,
      password: this.loginForm.get('password').value
    };
    this.loginSubscription = this.authService.login(credentials).subscribe(response => {
      // HTTP 200
      console.log('Got Response: ');
      console.log(response);
      this.router.navigate(['/users']);
      // Store token and change login status through observer
      localStorage.setItem('token', 'Basic ' + btoa(credentials.username + ':' + credentials.password));
    }, error => {
      // TODO: HTTP 401 should display error msg
      console.log('Error occurred: ');
      console.log(error['message']);
      console.log(error);
    });
  }
}
