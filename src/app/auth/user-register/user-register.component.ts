import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ISubscription} from 'rxjs/Subscription';
import {AuthService} from '../../shared/services/auth.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.scss']
})
export class UserRegisterComponent implements OnInit, OnDestroy {
  registerSubscription: ISubscription;
  signupForm: FormGroup;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.signupForm = new FormGroup({
      userData: new FormGroup({
        fName: new FormControl('', {
          validators: Validators.required,
          updateOn: 'change'
        }),
        sName: new FormControl('', {
          validators: Validators.required,
          updateOn: 'change'
        }),
        username: new FormControl('', {
          validators: Validators.required,
          updateOn: 'change'
        }),
        email: new FormControl('', {
          validators: [Validators.required, Validators.email],
          updateOn: 'change'
        }),
        password: new FormControl('', {
          validators: Validators.required,
          updateOn: 'change'
        }),
        confirmPassword: new FormControl('', {
          validators: Validators.required,
          updateOn: 'change'
        })
      }),
      acceptTerms: new FormControl(null),
      agreePrivacy: new FormControl(null)
    });
  }

  ngOnDestroy(): void {
    if (this.registerSubscription != null) {
      this.registerSubscription.unsubscribe();
    }
  }

  onSubmit() {
    const incomingData = this.signupForm.value.userData;
    const userInfoData = {
      'email': incomingData.email,
      'name': incomingData.fName,
      'password': incomingData.password,
      'username': incomingData.username
    };

    this.registerSubscription = this.authService.register(userInfoData).subscribe(response => {
      console.log('User created successfully!');
    }, error => {
      console.log(error);
    });


  }
}
