import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient, private router: Router) {
  }

  public register(userData: any): Observable<any> {
    // console.log('Register user info received: ' + userData);
    return this.http.post('http://x200.dynu.net:666/api/users', userData);
  }

  public login(credentials: any): Observable<any> {
    console.log('Login user info received: ' + credentials.username + ' / ' + credentials.password);
    // Create Request headers including encoded auth
    const headers = new HttpHeaders(credentials ? {
      Authorization: 'Basic ' + btoa(credentials.username + ':' + credentials.password)
    } : {});
    // Send API Request using provided info and return response as observable
    return this.http.get('http://x200.dynu.net:666/api/users', {
      headers: headers
    });
  }

  public logout() {
    console.log('Auth Service logout');
    if (localStorage.getItem('token') != null) {
      localStorage.removeItem('token');
      console.log('Auth token cleared.');
      this.router.navigate(['/login']);
    }
  }
}
